//
// Create by Zeng Yun on 2019/1/8
//

package member

import (
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/certificate"
	"testing"
)

func Test_PushNotificationToMember(t *testing.T) {
	id := "ce173611-1d3d-4414-94ff-1764fc7812f3"
	aps := &APNSAlert{
		Title: "7点整回家",
		Body:  "今天已经是1月8号了，你知道吗？",
	}
	err := PushNotificationToMember(id, aps)
	if err != nil {
		t.Fatal(err)
	}
}

func Test_APNSClient(t *testing.T) {
	cert, err := certificate.FromP12File("../conf/apns.p12", "clouds")
	if err != nil {
		t.Fatal(err)
	}

	notification := &apns2.Notification{
		DeviceToken: "1912510553fac4197c2efec36624555994f77ca7550f7535c55469e790f58c76",
		Topic:       "adai.design.home",
		Payload: []byte(`{
			"aps" : {
				"alert" : {
					"title" : "7点整回家",
					"body" : "今天已经是1月8号了，你知道吗？"
				},
				"apns-priority": 5,
				"content-available": 1,
				"category":"GENERAL",
				"badge" : 1,
				"sound":"default"
	    	}
		}`),
	}

	client := apns2.NewClient(cert).Development()
	res, err := client.Push(notification)
	if err != nil {
		t.Fatal("result err: ", err)
	}
	t.Logf("result code(%v) apnsId(%v) reason(%v)", res.StatusCode, res.ApnsID, res.Reason)
}
