package member

import (
	"adai.design/homeserver/members"
	"adai.design/jarvis/common/log"
	"adai.design/jarvis/home/model"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"testing"
	"time"
)

const (
	testServerURL  = "ws://adai.design:6666/member/ws"
	testAccount    = "785492565@qq.com"
	testHome       = "187c7894-1d3d-442b-9d3a-d2ebcfa86a6c"
	testAccountPwd = "123456"
)

// 测试客户单
type client struct {
	url      string
	account  string
	password string
	id       string
	session  string
	devType  string
	conn     *websocket.Conn
}

func (c *client) writeMsg(msg *Message) error {
	buf, _ := json.Marshal(msg)
	log.InfoTo(">>>>>> %s", string(buf))
	return c.conn.WriteMessage(websocket.BinaryMessage, buf)
}

func (c *client) readMessage() (*Message, error) {
	_, buf, err := c.conn.ReadMessage()
	if err != nil {
		return nil, err
	}

	var msg Message
	err = json.Unmarshal(buf, &msg)
	if err != nil {
		return nil, err
	}
	log.InfoFrom("<<<<< %s", string(buf))
	return &msg, nil
}

func (c *client) login() error {

	data := loginRequest{
		Account:  c.account,
		Password: c.password,
		Id:       c.id,
		Token:    c.session,
		DevType:  c.devType,
	}

	if data.Id != "" {
		data.Account = ""
		data.Password = ""
	}

	msg := &Message{
		Path:   msgPathLogin,
		Method: MsgMethodPost,
	}
	msg.Data, _ = json.Marshal(data)

	err := c.writeMsg(msg)
	if err != nil {
		return err
	}

	// 读取登录返回消息
	msg, err = c.readMessage()
	if err != nil {
		return err
	}

	if msg.Path != msgPathLogin || msg.Method != MsgMethodPost {
		return fmt.Errorf("login failed <- %s", msg)
	}
	if msg.State != "ok" {
		return fmt.Errorf("login failed with reason <- %s", msg.State)
	}

	var response loginResponse
	err = json.Unmarshal(msg.Data, &response)
	if err != nil {
		return err
	}

	c.id = response.Id
	c.session = response.Token
	return nil
}

func (c *client) close() {
	c.conn.Close()
}

func startAndLogin(account, password string) (*client, error) {
	c := &client{
		url:      testServerURL,
		account:  account,
		password: password,
		devType:  ClientTypePC,
	}

	// 第一次登录 获取id+token
	log.Info("mark:before")
	var err error
	c.conn, _, err = websocket.DefaultDialer.Dial(c.url, nil)
	if err != nil {
		return nil, err
	}
	log.Info("mark:after")

	err = c.login()
	if err != nil {
		c.conn.Close()
		return nil, fmt.Errorf("client(%s:%s) login failed", c.account, c.devType)
	}
	return c, nil
}

func startReception(c *client, end chan error) {
	var err error
	c.conn, _, err = websocket.DefaultDialer.Dial(c.url, nil)
	if err != nil {
		end <- fmt.Errorf("client(%s:%s) dial(%s) failed", c.account, c.devType, c.url)
		return
	}
	defer c.close()

	err = c.login()
	if err != nil {
		end <- fmt.Errorf("client(%s:%s) login failed", c.account, c.devType)
		return
	}

	for {
		select {
		case _, ok := <-end:
			if !ok {
				return
			}

		case <-time.After(time.Second * 3):
			err = c.conn.WriteControl(websocket.PingMessage, nil, time.Now().Add(idleTimeout))
			if err != nil {
				end <- fmt.Errorf("client(%s:%s) heartbeat error", c.account, c.devType)
				return
			}
		}
	}
}

// 不通客户端登录测试
func TestReceptionLogin(t *testing.T) {
	end := make(chan error, 1)
	defer close(end)

	for _, devType := range []string{ClientTypePhone, ClientTypePad, ClientTypePC, ClientTypeWeb} {
		c := &client{
			url:      testServerURL,
			account:  testAccount,
			password: testAccountPwd,
			devType:  devType,
		}
		go startReception(c, end)
		time.Sleep(time.Second)
	}
}

// 测试APNS信息推送Token更新
func TestReceptionPushAPNSInfo(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   msgPathMemberAPNS,
		Method: MsgMethodPost,
	}
	apns := &apnsInfo{
		Topic:   "adai.design.home",
		APNS:    "8297a92e1614e7cec61af84bb68c09504f99fd36f92d065b7f1bb3557be8442d",
		DevType: "pad",
	}
	msg.Data, _ = json.Marshal(apns)
	client.writeMsg(msg)

	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

// 家庭信息获取
func TestAccountHomeInfoGet(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home/info",
		Method: "get",
		Home:   testHome,
	}
	_ = client.writeMsg(msg)

	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

// 家庭数据获取
func TestAccountHomeGet(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home",
		Method: "get",
		Home:   testHome,
	}
	client.writeMsg(msg)

	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
	var home model.HMHome
	err = json.Unmarshal(msg.Data, &home)
	if err != nil {
		t.Fatal(err)
	}

	buf, _ := json.MarshalIndent(&home, "", "    ")
	err = ioutil.WriteFile("../tmp/home.json", buf, 0666)
	if err != nil {
		t.Fatal(err)
	}
	//t.Logf("\n%s", string(buf))
}

// 更新家庭数据
func TestHomePost(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	buf, err := ioutil.ReadFile("../tmp/home.json")
	if err != nil {
		t.Fatal(err)
	}

	msg := &Message{
		Path:   "home",
		Method: "post",
		Home:   testHome,
		Data:   buf,
	}
	_ = client.writeMsg(msg)

	// 推送成功的回复
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())

	if msg.State != "ok" {
		t.Fatal("post failed")
	}

	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

// 获取容器数据
func TestContainerGet(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home/container",
		Method: "get",
		Home:   testHome,
	}
	client.writeMsg(msg)

	// 推送成功的回复
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

// 获取容器属性
func TestContainerCharacteristicGet(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home/container/characteristic",
		Method: "get",
		Home:   testHome,
	}
	client.writeMsg(msg)

	// 推送成功的回复
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

// 获取容器属性
func TestHomeMemberGet(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home/member",
		Method: "get",
		Home:   testHome,
	}
	client.writeMsg(msg)

	// 推送成功的回复
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

func TestHomeMemberStatePost(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	msg := &Message{
		Path:   "home/member/location",
		Method: "post",
		Home:   testHome,
	}
	msg.Data, _ = json.Marshal(map[string]interface{}{
		"member": "ce173611-1d3d-4414-94ff-1764fc7812f3",
		"state":  "arrive",
	})
	client.writeMsg(msg)

	// 推送成功的回复
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

//5c1f8f83289cb477e1ddb053

// 更新容器属性,设备操作
func TestContainerCharacteristicPost(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	actions := []*model.Action{
		{
			AId:   "5c1f8f83289cb477e1ddb053",
			SId:   20,
			CId:   6,
			Value: 100,
		},
	}
	buf, _ := json.Marshal(actions)
	msg := &Message{
		Path:   "home/characteristic",
		Method: members.MsgMethodPost,
		Home:   testHome,
		Data:   buf,
	}

	client.writeMsg(msg)
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}

func TestSceneExecute(t *testing.T) {
	client, err := startAndLogin(testAccount, testAccountPwd)
	if err != nil {
		t.Fatal(err)
	}
	defer client.close()

	post := map[string]interface{}{
		"type": "execute",
		//"scene": "43ddede5-7a9f-4b7d-878f-d4be3e349b61",
		"scene": "950d79c1-24e0-479e-975b-f3300c533ab7",
	}
	buf, _ := json.Marshal(post)
	msg := &Message{
		Path:   "home/scene",
		Method: members.MsgMethodPost,
		Home:   testHome,
		Data:   buf,
	}

	client.writeMsg(msg)
	msg, err = client.readMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("read message <- %s", msg.String())
}
