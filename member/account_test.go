package member

import (
	"testing"
)

// 查找所有账号
func TestAccount_FindAll(t *testing.T) {
	as, err := accounts.findAll()
	if err != nil {
		t.Fatal(err)
	}
	for _, a := range as {
		if a.Phone != "" {
			t.Logf("account(%s) id(%s) name(%s)", a.Phone, a.Id, a.Name)
		} else {
			t.Logf("account(%s) id(%s) name(%s)", a.Email, a.Id, a.Name)
		}
	}
}

// 插入新账号
func TestAccountInsert(t *testing.T) {
	phone := "785492565@qq.com"
	password := "123456"

	account, err := accounts.Insert(phone, password)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(account)
}

// 修改账号信息
func TestAccountModify(t *testing.T) {
	email := "785492565@qq.com"
	account, err := accounts.FindByAccount(email)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("account = ", account)
	account.Email = "785492565@qq.com"
	account.Name = "穿着裤子的云"
	account.Icon = "#daodao"

	err = accounts.Update(account)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(account)
}

// 测试登录
func TestAccountLogin(t *testing.T) {
	email := "785492565@qq.com"
	password := "123456"
	devType := ClientTypePad

	account, client, err := accounts.Login(email, password, devType)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(account)
	t.Log(client)

	account, client, err = accounts.SessionCheck(account.Id, client.Session, devType)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(account)
	t.Log(client)
}

func TestAccountManager_SessionCheck(t *testing.T) {

	uid := ""
	session := ""
	devType := "pc"

	account, client, err := accounts.SessionCheck(uid, session, devType)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(account)
	t.Log(client)
}
