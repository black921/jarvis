package device

import "encoding/json"

type Message struct {
	Path   string          `json:"path"`
	Method string          `json:"method"`
	State  string          `json:"state,omitempty"`
	Data   json.RawMessage `json:"data,omitempty"`
}

func (m *Message) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}

const (
	msgPathLogin                   = "login"
	msgPathHeartbeat               = "hb"
	msgPathBindInfo                = "bind"
	MsgPathContainer               = "container"
	MsgPathContainerCharacteristic = "characteristics"
	MsgPathContainerInfo           = "container/info"
)

const (
	MsgMethodPost = "post"
	MsgMethodGet  = "get"
	MsgMethodPut  = "put"
)

// 设备与家庭中心之间的通信消息结构体
type MessagePkg struct {
	DevId  string   `json:"device"`
	HomeId string   `json:"home"`
	Msg    *Message `json:"msg"`
}

func (m *MessagePkg) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}

// 设备容器的在线离线，加入家庭，离开家庭，状态更新
const (
	ContainerStateOnline  = "online"
	ContainerStateOffline = "offline"
	ContainerStateJoin    = "join"
	ContainerStateRemove  = "remove"
	ContainerStateUpdate  = "update"
)
