package device

import (
	"testing"
)

func TestRegisterFormat(t *testing.T) {
	t.Log("let's go!")

	for i := 0; i < 1; i++ {
		r, err := registers.register("master")
		if err != nil {
			t.Fatal(err)
		}
		r.Save()

		reg, err := registers.findRegisterById(r.Id)
		if err != nil {
			t.Fatal(err)
		}

		t.Logf("%s", reg)
		t.Log(r)
	}

}

func TestRegisterFindAll(t *testing.T) {
	t.Log("let's go!")

	rs, err := registers.findRegisterAll()
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("%v", rs)

}
