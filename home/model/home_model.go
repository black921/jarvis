//
// Create by Zeng Yun on 2018/12/20
//

package model

import "encoding/json"

// 家庭简要信息
type HomeInfo struct {
	Id      string `json:"id" bson:"id"`
	Name    string `json:"name" bson:"name"`
	Version int    `json:"version" bson:"version"`

	Members []*Member `json:"members,omitempty" bson:"members,omitempty"`
}

func (h *HomeInfo) String() string {
	buf, _ := json.Marshal(h)
	return string(buf)
}

type HMHome struct {
	HomeInfo `bson:",inline"`

	// 容器
	Containers []*Container `json:"containers,omitempty" bson:"containers,omitempty"`

	// 房间
	Rooms []*Room `json:"rooms,omitempty" bson:"rooms,omitempty"`

	// 服务
	Services []*Service `json:"services,omitempty" bson:"services,omitempty"`

	// 场景
	Scenes []*Scene `json:"scenes,omitempty" bson:"scenes,omitempty"`

	// 自动化
	Automations []*Automation `json:"automations,omitempty" bson:"automations,omitempty"`
}

func (h *HMHome) String() string {
	buf, _ := json.Marshal(h)
	return string(buf)
}
