//
// Create by Zeng Yun on 2018/12/24
//

package home

import (
	"adai.design/jarvis/home/model"
	"encoding/json"
	"io/ioutil"
	"testing"
)

func TestHome_DataParse(t *testing.T) {
	data, err := ioutil.ReadFile("home.json")
	if err != nil {
		t.Fatal(err)
	}

	var home model.HMHome
	err = json.Unmarshal(data, &home)
	if err != nil {
		t.Fatal(err)
	}
	//t.Log(&home)

	for _, s := range home.Scenes {
		t.Logf("scene(%s) id(%s) type(%s) actions(%d)", s.Name, s.Id, s.Type, len(s.Actions))
	}

	for _, r := range home.Rooms {
		t.Logf("room(%s) id(%s)", r.Name, r.Id)
	}
}
